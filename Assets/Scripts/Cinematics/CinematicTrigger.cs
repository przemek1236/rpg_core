﻿using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematics
{

    public class CinematicTrigger : MonoBehaviour
    {
        bool wasPlayed = false;
        private void OnTriggerEnter(Collider other)
        {
            if (!wasPlayed && other.tag == "Player")
            {
                GetComponent<PlayableDirector>().Play();
                wasPlayed = true;
            }
        }
    }
}

