﻿using UnityEngine;
using TMPro;
using RPG.Resources;

namespace RPG.Combat
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        private Fighter fighter;
        
        TextMeshProUGUI gui;

        private void Awake()
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
            gui = GetComponent<TextMeshProUGUI>();
        }
        private void Update()
        {
            if (fighter.GetTarget() != null)
            {
                gui.text = Mathf.RoundToInt(fighter.GetTarget().GetPercentage()).ToString() + "%";
            }
            else
            {
                gui.text = "";
            }
        }
    }
}
