﻿using UnityEngine;
using TMPro;

namespace RPG.Resources
{
    public class HealthDisplay : MonoBehaviour
    {
        private Health health;
        TextMeshProUGUI gui;

        private void Awake()
        {
            health = GameObject.FindWithTag("Player").GetComponent<Health>();
            gui = GetComponent<TextMeshProUGUI>();
        }
        private void Update()
        {
            gui.text = Mathf.RoundToInt(health.GetPercentage()).ToString() + "%";
        }
    }
}
