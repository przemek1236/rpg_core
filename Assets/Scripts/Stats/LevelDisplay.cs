﻿using UnityEngine;
using TMPro;

namespace RPG.Stats
{
    public class LevelDisplay : MonoBehaviour
    {
        private BaseStats baseStats;
        TextMeshProUGUI gui;

        private void Awake()
        {
            baseStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
            gui = GetComponent<TextMeshProUGUI>();
        }
        private void Update()
        {
            gui.text = baseStats.GetLevel().ToString();
        }
    }
}
