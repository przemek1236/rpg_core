﻿using UnityEngine;
using TMPro;

namespace RPG.Stats
{
    public class ExperienceDisplay : MonoBehaviour
    {
        private Experience experience;
        TextMeshProUGUI gui;

        private void Awake()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
            gui = GetComponent<TextMeshProUGUI>();
        }
        private void Update()
        {
            gui.text = experience.GetExperiencePoints().ToString();
        }
    }
}
